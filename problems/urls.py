from django.urls import path

from . import views

app_name = "problems"
urlpatterns = [
    path("", views.index, name="index"),
    path("new/", views.new_problem, name="new"),
    path("search/", views.search, name="search"),
    path("<int:problem_id>/", views.detail, name="detail"),
    path("<int:problem_id>/edit/", views.edit_problem, name="edit"),
]
