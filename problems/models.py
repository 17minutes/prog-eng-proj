from django.db import models
from django.contrib.auth.models import User


class Tag(models.Model):
    tag_name = models.CharField(max_length=32)

    def __str__(self):
        return self.tag_name


def get_default_user():
    default_user = User.objects.filter(username="ksidorov").first()
    assert default_user is not None
    return default_user.pk


class Problem(models.Model):
    question_text = models.TextField()
    answer_text = models.TextField()
    submitter = models.ForeignKey(
        User, on_delete=models.CASCADE, default=get_default_user
    )
    pub_date = models.DateTimeField(auto_now=True)
    tags = models.ManyToManyField(Tag)

    @property
    def tag_string(self):
        return ", ".join((str(x) for x in self.tags.all()))
