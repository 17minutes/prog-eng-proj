# Generated by Django 3.0.1 on 2019-12-27 22:38

from django.conf import settings
from django.db import migrations, models
import django.db.models.deletion
import problems.models


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
        ('problems', '0003_auto_20191227_1016'),
    ]

    operations = [
        migrations.AddField(
            model_name='problem',
            name='submitter',
            field=models.ForeignKey(default=problems.models.get_default_user, on_delete=django.db.models.deletion.CASCADE, to=settings.AUTH_USER_MODEL),
        ),
    ]
