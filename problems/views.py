from django.shortcuts import get_object_or_404, render, redirect
from django.http import HttpResponse, HttpResponseRedirect, JsonResponse
from django.template import loader
from django.urls import reverse
from django.views.decorators.http import require_http_methods, require_GET
from django.contrib.auth.decorators import login_required

from .models import Problem, Tag

import re


def index(request):
    problems = Problem.objects
    try:
        tag = Tag.objects.get(tag_name=request.GET.get("tag", None))
        problems = problems.filter(tags=tag)
    except Tag.DoesNotExist:
        tag = None
    problems = problems.order_by("-pub_date")
    return render(request, "problems/index.html", {"problems": problems, "tag": tag})


def extract_tags(tag_string):
    tag_strings = [x.strip().lower() for x in tag_string.split(",") if x.strip() != ""]
    return [
        Tag.objects.get_or_create(tag_name=tag_string)[0] for tag_string in tag_strings
    ]


@login_required
def edit_problem(request, problem_id):
    problem = Problem.objects.filter(pk=problem_id).first()
    if problem is None:
        return redirect(reverse("problems:new"))
    if request.user != problem.submitter:
        return HttpResponseRedirect(reverse('problems:index'))
    if request.method == "POST":
        question = request.POST.get("question", "")
        answer = request.POST.get("answer", "")

        if question != "" and answer != "":
            problem.question_text = question
            problem.answer_text = answer
            problem.save()
            problem.tags.set(extract_tags(request.POST.get("tags", "")))
            return HttpResponseRedirect(reverse("problems:detail", args=(problem_id,)))
        else:
            if question == "":
                error_msg = "Пожалуйста, запишите условие задачи."
            else:
                error_msg = "Пожалуйста, укажите ответ."
            return render(
                request,
                "problems/editor.html",
                {
                    "question_text": question,
                    "answer_text": answer,
                    "error_message": error_msg,
                },
            )

    if request.method == "GET":
        return render(request, "problems/editor.html", {"target_problem": problem})


@login_required
def new_problem(request):
    if request.method == "POST":
        question = request.POST.get("question", "")
        answer = request.POST.get("answer", "")
        if question != "" and answer != "":
            new_problem = Problem(question_text=question, answer_text=answer, submitter=request.user)
            new_problem.save()
            new_problem.tags.set(extract_tags(request.POST.get("tags", "")))
            return HttpResponseRedirect(
                reverse("problems:detail", args=(new_problem.id,))
            )
        else:
            if question == "":
                error_msg = "Пожалуйста, запишите условие задачи."
            else:
                error_msg = "Пожалуйста, укажите ответ."
            return render(
                request,
                "problems/editor.html",
                {
                    "question_text": question,
                    "answer_text": answer,
                    "error_message": error_msg,
                },
            )

    if request.method == "GET":
        return render(request, "problems/editor.html")


def detail(request, problem_id):
    problem = get_object_or_404(Problem, pk=problem_id)

    return render(request, "problems/detail.html", {"problem": problem})


@require_GET
def search(request):
    search_string = request.GET.get("q", "")
    tags = []
    matches = list(re.finditer(r"tag:(?P<tag>\w+)\s?", search_string))
    for m in matches:
        tag = Tag.objects.filter(tag_name=m.group("tag")).first()
        if tag is not None:
            tags.append(tag)
        search_string = search_string.replace(m.group(), "")
    res = Problem.objects.filter(question_text__icontains=search_string)
    for tag in tags:
        res = res.filter(tags=tag)
    return JsonResponse(
        {
            "status": "ok",
            "problems": [{"id": x.pk, "text": x.question_text} for x in res],
        }
    )
