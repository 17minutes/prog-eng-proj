from django.shortcuts import get_object_or_404, render, redirect
from django.http import HttpResponse, HttpResponseRedirect
from django.template import loader
from django.urls import reverse
from django.views.decorators.http import require_http_methods, require_POST
from django.contrib.auth.models import User
from django.apps import apps

from .models import QuizEntry, Quiz, QuizVariant


def index(request, username=None):
    quizzes = Quiz.objects
    user = None
    if username is not None:
        user = get_object_or_404(User, username=username)
        quizzes = quizzes.filter(editor=user)
    quizzes = quizzes.order_by("-pub_date")
    return render(request, "quizzes/index.html", {"quizzes": quizzes, "editor": user})


@require_POST
def new_quiz(request):
    quiz = Quiz()
    quiz.save()
    return HttpResponseRedirect(reverse("quizzes:detail", args=(quiz.id,)))


def detail(request, quiz_id):
    quiz = get_object_or_404(Quiz, pk=quiz_id)
    return render(request, "quizzes/detail.html", {"quiz": quiz})


def delete_quiz(request, quiz_id):
    quiz = get_object_or_404(Quiz, pk=quiz_id)
    quiz.delete()
    return HttpResponseRedirect(reverse("quizzes:index"))


@require_POST
def rename_quiz(request, quiz_id):
    quiz = get_object_or_404(Quiz, pk=quiz_id)
    new_title = request.POST["title"]
    quiz.title = new_title
    quiz.save()
    return HttpResponse(status=200)


@require_POST
def modify_quiz_entry(request, quiz_id, var_index, entry_index):
    quiz = get_object_or_404(Quiz, pk=quiz_id)
    quiz_var = quiz.variants[var_index - 1]
    order = list(quiz_var.entry_ids)
    if "up" in request.POST:
        order[entry_index - 2], order[entry_index - 1] = (
            order[entry_index - 1],
            order[entry_index - 2],
        )
        quiz_var.set_quizentry_order(order)
    elif "down" in request.POST:
        order[entry_index - 1], order[entry_index] = (
            order[entry_index],
            order[entry_index - 1],
        )
        quiz_var.set_quizentry_order(order)
    elif "remove" in request.POST:
        quiz_var.entries[entry_index - 1].delete()
    return HttpResponseRedirect(reverse("quizzes:detail", args=(quiz_id,)))


@require_POST
def add_quiz_variant(request, quiz_id):
    quiz = get_object_or_404(Quiz, pk=quiz_id)
    quiz_var = QuizVariant(quiz=quiz)
    quiz_var.save()
    return HttpResponseRedirect(reverse("quizzes:detail", args=(quiz_id,)))


@require_POST
def modify_quiz_variant(request, quiz_id, var_index):
    Problem = apps.get_model("problems", "Problem")
    quiz = get_object_or_404(Quiz, pk=quiz_id)
    quiz_var = quiz.variants[var_index - 1]
    if "insert" in request.POST:
        problem_id = request.POST.get("problem", None)
        problem = Problem.objects.get(pk=problem_id)
        if QuizEntry.objects.filter(quiz_var=quiz_var, problem=problem).count() == 0:
            quiz_entry = QuizEntry(quiz_var=quiz_var, problem=problem)
            quiz_entry.save()
    elif "remove" in request.POST:
        quiz_var.delete()
    return HttpResponseRedirect(reverse("quizzes:detail", args=(quiz_id,)))
