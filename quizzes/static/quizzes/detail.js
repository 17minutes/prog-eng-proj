function autoresize(textarea) {
    var value = textarea.val();
    var div = $('<h1>').hide().text(value).appendTo('body');
    textarea.width(div.width() * 1.15);
    div.remove();
}

$(document).ready(function ($) {
    $("#read-only-toggle").change(function () {
        if ($(this).prop('checked')) {
            $(".quiz-edit-control").hide();
            $(".quiz-problem-selector").hide();
        }
        else
            $(".quiz-edit-control").show();
    });

    $(".quiz-problem-selector > div.form-inline > button").click(function () {
        var search = $(this).data("href");
        var container = $(this).parent().parent();
        $.get(
            search,
            { q: container.find("input[name=q]").val() },
            function (data, textStatus, jqXHR) {
                var searchOut = container.find("tbody");
                searchOut.empty();
                for (const x of data.problems) {
                    var addButton = $("<button>")
                        .addClass("btn")
                        .addClass("btn-success")
                        .html("Выбрать")
                        .click(function () {
                            $.post(
                                container.data("href"),
                                { "insert": true, "problem": x.id },
                                function (err, params) {
                                    window.location.reload(true);
                                }
                            );
                        });
                    searchOut.append(
                        $("<tr></tr>").append(
                            `<td>${x.id}</td>`
                        ).append(
                            `<td>${x.text}</td>`
                        ).append(
                            `<td>${x.text}</td>`
                        ).append($("<td>").append(addButton))
                    );
                }
            }
        );
    });

    $(".quiz-newtask-toggle").click(function () {
        $(".quiz-problem-selector").hide();
        var variant = $(this).data('index');
        $(`#problem-selector-${variant}`).show();
    });

    autoresize($('#quiz-title-editor'));
    $('#quiz-title-editor').on('input propertychange paste', function () {
        autoresize($(this));
    });

    var renaming = false;

    $("#rename-btn").click(function () {
        $("#quiz-title").toggle();
        $("#quiz-title-editor").toggle();
        if (renaming) {
            var renamePath = $(this).data("href");
            $.post(
                renamePath,
                { title: $('#quiz-title-editor').val() },
                function (err, params) {
                    window.location.reload(true);
                }
            );
        }
        else {
            $("#rename-btn")
                .removeClass("btn-outline-secondary")
                .addClass("btn-outline-success")
                .html('<i class="fas fa-check"></i>');
        }
        renaming = !renaming;
    });
});
