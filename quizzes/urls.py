from django.urls import path

from . import views

app_name = "quizzes"
urlpatterns = [
    path("", views.index, name="index"),
    path("user/<username>/", views.index, name="index"),
    path("new/", views.new_quiz, name="new"),
    path("<int:quiz_id>/", views.detail, name="detail"),
    path("<int:quiz_id>/delete/", views.delete_quiz, name="delete"),
    path("<int:quiz_id>/rename", views.rename_quiz, name="rename"),
    path(
        "<int:quiz_id>/<int:var_index>/modify/<int:entry_index>",
        views.modify_quiz_entry,
        name="modify-entry",
    ),
    path("<int:quiz_id>/add-variant", views.add_quiz_variant, name="add-variant",),
    path(
        "<int:quiz_id>/<int:var_index>/modify",
        views.modify_quiz_variant,
        name="modify-variant",
    ),
]
