from django.contrib import admin
from .models import Quiz, QuizEntry, QuizVariant

# Register your models here.
admin.site.register(Quiz)
admin.site.register(QuizVariant)
admin.site.register(QuizEntry)
