from django.db import models
from django.contrib.auth.models import User


def get_default_user():
    default_user = User.objects.filter(username="ksidorov").first()
    assert default_user is not None
    return default_user.pk


class Quiz(models.Model):
    pub_date = models.DateTimeField(auto_now=True)
    title = models.CharField(default="Новая контрольная", max_length=32)
    editor = models.ForeignKey(User, on_delete=models.CASCADE, default=get_default_user)

    @property
    def variant_ids(self):
        return self.get_quizvariant_order()

    @property
    def variants(self):
        return QuizVariant.objects.filter(pk__in=self.get_quizvariant_order())


class QuizVariant(models.Model):
    quiz = models.ForeignKey(Quiz, null=False, on_delete=models.CASCADE)

    @property
    def entry_ids(self):
        return self.get_quizentry_order()

    @property
    def entries(self):
        return QuizEntry.objects.filter(pk__in=self.get_quizentry_order())

    class Meta:
        order_with_respect_to = "quiz"

    def save(self, *args, **kwargs):
        instance = super(QuizVariant, self).save(*args, **kwargs)
        self.quiz.save()
        return instance


class QuizEntry(models.Model):
    quiz_var = models.ForeignKey(QuizVariant, null=False, on_delete=models.CASCADE)
    problem = models.ForeignKey(
        "problems.Problem", null=False, on_delete=models.CASCADE
    )

    class Meta:
        order_with_respect_to = "quiz_var"

    def save(self, *args, **kwargs):
        instance = super(QuizEntry, self).save(*args, **kwargs)
        self.quiz_var.save()
        return instance
